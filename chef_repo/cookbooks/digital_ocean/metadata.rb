name 'digital_ocean'
maintainer 'The Authors'
maintainer_email 'you@example.com'
license 'all_rights'
description 'Installs/Configures digital_ocean'
long_description 'Installs and configures a hardened server for anything running on a modern version of Ubuntu.'
version '1.9.0'

# If you upload to Supermarket you should set this so your cookbook
# gets a `View Source` link
# source_url 'https://github.com/<insert_org_here>/digital_ocean' if respond_to?(:source_url)
