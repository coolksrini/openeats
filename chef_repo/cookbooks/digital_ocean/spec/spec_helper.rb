require 'chefspec'
ChefSpec::Coverage.start!

RSpec.configure do |config|
    config.before(:each) do
        class ::String 
            def read
                self.to_s
            end
        end
        allow(::File).to receive(:open).and_return('secret!')
        stub_data_bag_item('sysadmins','digital_ocean_droplet')
            .and_return({:from => 'me',:to => 'you',:domain => 'example.com'})
    end
end