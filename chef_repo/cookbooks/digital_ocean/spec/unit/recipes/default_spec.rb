#
# Cookbook Name:: digital_ocean
# Spec:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

require 'spec_helper'

describe 'digital_ocean::default' do
    context 'When running against an Ubuntu 16.04 platform' do
        let(:chef_run) { ChefSpec::SoloRunner.new(platform: 'ubuntu',version: '16.04').converge(described_recipe) }
        
        it 'should create a devops user' do
            expect(chef_run).to create_user('devops').with_shell('/bin/bash')
            expect(chef_run).to create_directory('/home/devops/.ssh')
            expect(chef_run).to create_file('/home/devops/.ssh/authorized_keys')
                .with_mode('400').
                with_group('devops')
            expect(chef_run).to render_file('/home/devops/.ssh/authorized_keys').with_content('secret!')
        end
        
        it 'should update the sudoers and sshd_config files and restart ssh to incorporate the changes' do
            expect(chef_run).to create_template('/etc/sudoers')
            expect(chef_run).to render_file('/etc/sudoers')
                .with_content('devops ALL=(ALL:ALL) NOPASSWD: ALL')
            
            expect(chef_run).to create_template('/etc/ssh/sshd_config')
            expect(chef_run).to render_file('/etc/ssh/sshd_config').with_content { |content|
                expect(content).to include('PasswordAuthentication no')
                expect(content).to include('PubkeyAuthentication yes')
                expect(content).to include('PermitRootLogin no')
                expect(content).to include('AllowUsers devops')
            }
            
            expect(chef_run).to restart_service('ssh')
        end
        
        it 'should enable ports 22,80, and 443' do
            expect(chef_run).to run_execute('sudo ufw allow 22')
            expect(chef_run).to run_execute('sudo ufw allow 80')
            expect(chef_run).to run_execute('sudo ufw allow 443')
        end
        
        it 'should disable and re-enable ufw' do
            expect(chef_run).to run_execute('sudo ufw --force disable')
            expect(chef_run).to run_execute('sudo ufw --force enable')
        end
        
        it 'should install unattended security upgrades' do
            expect(chef_run).to install_package('unattended-upgrades')
            expect(chef_run).to create_cookbook_file('/etc/apt/apt.conf.d/10periodic')
            expect(chef_run).to create_cookbook_file('/etc/apt/apt.conf.d/50unattended-upgrades')
            expect(chef_run).to render_file('/etc/apt/apt.conf.d/50unattended-upgrades').with_content { |content|
                expect(content).to match(/Unattended-Upgrade::Allowed-Origins {/)
                expect(content).to match(%r[//\s*"\${distro_id}:\${distro_codename}";])
                expect(content).to match(%r["\${distro_id}:\${distro_codename}-security";])
            }
        end
        
        it 'should install fail2ban' do
            expect(chef_run).to install_package('fail2ban')
        end
      
        it 'should install logwatch' do
            expect(chef_run).to install_package('logwatch')
            expect(chef_run).to render_file('/usr/share/logwatch/default.conf/logwatch.conf').with_content { |content|
                expect(content).to include('MailFrom = me')
                expect(content).to include('MailTo = you')
            }
        end
  end
end
