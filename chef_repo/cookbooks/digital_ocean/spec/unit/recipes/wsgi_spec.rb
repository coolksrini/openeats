require 'spec_helper'

describe 'digital_ocean::wsgi' do
    context 'When running against an Ubuntu 16.04 platform' do
        let(:chef_run) { ChefSpec::SoloRunner.new(platform: 'ubuntu',version: '16.04').converge(described_recipe) }

        it 'should include the default recipe' do
            expect(chef_run).to include_recipe('digital_ocean::default')
        end
        
        it 'should install apache2' do
            expect(chef_run).to install_package('apache2')
        end
        
        it 'should install the mod-wsgi module' do
            expect(chef_run).to install_package('libapache2-mod-wsgi')
        end
        
        it 'should install python dependencies' do
            expect(chef_run).to install_package('python-pip')
            expect(chef_run).to install_package('virtualenv')
        end
        
        it 'should install dependencies necessary for compiled C libraries' do
            expect(chef_run).to install_package('python-dev')
            expect(chef_run).to install_package('build-essential')
        end
        
        it 'should upgrade pip to the latest version' do
            expect(chef_run).to run_execute('sudo pip install --upgrade pip')
        end
        
        it 'should create the appropriate apache directory' do
            expect(chef_run).to create_directory('/var/www/home_bistro')
        end
        
        it 'should install a virtual environment in the apache directory' do
            expect(chef_run).to run_execute('sudo virtualenv /var/www/home_bistro/venv --no-site-packages')
        end
        
        it "should enable the wsgi site and static content via home-bistro.conf" do
            expect(chef_run).to create_template('/etc/apache2/sites-available/home-bistro.conf')
            
            expect(chef_run).to render_file('/etc/apache2/sites-available/home-bistro.conf').with_content { |content|
                    expect(content).to match(/^\s*WSGIDaemonProcess home_bistro/)
                    expect(content).to include('ServerName example.com')
                    expect(content).to include('ServerAlias www.example.com')
                    expect(content).not_to match(/RewriteEngine on/)
                }
        end
        
        it 'should enable the home-bistro.conf VirtualHost' do
            expect(chef_run).to run_execute('sudo a2ensite home-bistro.conf')
        end
        
        it 'should install certbot' do
            expect(chef_run).to create_remote_file_if_missing('/usr/local/sbin/certbot-auto')
        end
        
        it 'should create an options-ssl conf file by default' do
            expect(chef_run).not_to create_cookbook_file('/etc/letsencrypt/options-ssl-apache.conf')
        end
        
        it 'should not create an ssl conf by default' do
            expect(chef_run).not_to create_template('/etc/apache2/sites-available/home-bistro-le-ssl.conf')
        end
        
        it 'should not run certbot to create a certificate by default' do
            expect(chef_run).to_not run_execute(
                "sudo /usr/local/sbin/certbot-auto certonly --apache -n -d example.com -m you --agree-tos --redirect")
        end
        
        it 'should not create a cron entry by default' do
            expect(chef_run).to_not create_cron('renew certificate')
        end
        
        it 'should enable the ssl module' do
            chef_run.node.force_default['certbot'] = true
            chef_run.converge(described_recipe)
            expect(chef_run).to run_execute('sudo a2enmod ssl')
        end
        
        it 'should enable the key caching' do
            chef_run.node.force_default['certbot'] = true
            chef_run.converge(described_recipe)
            expect(chef_run).to run_execute('sudo a2enmod socache_shmcb')
        end
        
        it 'should enable rewrite rules' do
            chef_run.node.force_default['certbot'] = true
            chef_run.converge(described_recipe)
            expect(chef_run).to run_execute('sudo a2enmod rewrite')
        end
        
        it 'should create a certificate if directed' do
            chef_run.node.force_default['certbot'] = true
            chef_run.converge(described_recipe)
            expect(chef_run).to run_execute(
                "sudo /usr/local/sbin/certbot-auto certonly --apache -n -d example.com -m you --agree-tos --redirect")
        end
        
        it 'should create an options-ssl conf if directed' do
            chef_run.node.force_default['certbot'] = true
            chef_run.converge(described_recipe)
            expect(chef_run).to create_cookbook_file('/etc/letsencrypt/options-ssl-apache.conf')
        end
        
        it 'should modify the original conf to drop thw WSGIDaemonProcess (to combat uniqueness problems) and establish rewrite rules' do
            chef_run.node.force_default['certbot'] = true
            chef_run.converge(described_recipe)
            expect(chef_run).to create_template('/etc/apache2/sites-available/home-bistro.conf')
            expect(chef_run).to render_file('/etc/apache2/sites-available/home-bistro.conf').with_content { |content|
                    expect(content).not_to match(/^\s*WSGIDaemonProcess home_bistro/)
                    expect(content).to match(/RewriteEngine on/)
                }            
        end
                        
        it 'should install the ssl conf module if directed' do
            chef_run.node.force_default['certbot'] = true
            chef_run.converge(described_recipe)
            expect(chef_run).to create_template('/etc/apache2/sites-available/home-bistro-le-ssl.conf').with_content { |content| 
                expect(content).to include('<IfModule mod_ssl.c>')
                expect(content).to match(/^\s*WSGIDaemonProcess home_bistro/)
                expect(content).to match(%r[SSLCertificateFile /etc/letsencrypt/live/example.com/fullchain.pem])
            }
        end
                
        it 'should enable the virtual host for the ssl conf' do
            chef_run.node.force_default['certbot'] = true
            chef_run.converge(described_recipe)
            expect(chef_run).to run_execute('sudo a2ensite home-bistro-le-ssl.conf')
        end
        
        it 'should create a cron entry if directed' do
            chef_run.node.force_default['certbot'] = true
            chef_run.converge(described_recipe)
            expect(chef_run).to create_cron('renew certificate')
                .with_minute('30')
                .with_hour('2')
                .with_user('devops')
                .with_mailto('you')
                .with_command('/usr/local/sbin/certbot-auto renew >> /var/log/le-renew.log')
        end
                
        it 'should restart the apache2 service' do
            expect(chef_run).to restart_service('apache2')
        end
    end
end
