include_recipe 'digital_ocean::default'

package 'apache2'
package 'libapache2-mod-wsgi'

package 'python-pip'
package 'virtualenv'

package 'python-dev'
package 'build-essential'

execute 'Upgrade pip' do
    command "sudo pip install --upgrade pip"
end

directory '/var/www/home_bistro' do
    action :create
end

execute 'Install venv' do
    command "sudo virtualenv /var/www/home_bistro/venv --no-site-packages"
end

droplet_data = data_bag_item('sysadmins','digital_ocean_droplet')
certificate_available = node['certbot']
template '/etc/apache2/sites-available/home-bistro.conf' do
    source 'home-bistro.conf.erb'
    action :create
    variables({
        :from => droplet_data['from'],
        :domain => droplet_data['domain'],
        :certificate_available => certificate_available
        })
end

execute 'Enable VirtualHost' do
    command 'sudo a2ensite home-bistro.conf'
end

remote_file '/usr/local/sbin/certbot-auto' do
    source 'https://dl.eff.org/certbot-auto'
    mode '0770'
    action :create_if_missing
end

# Use this with knife ssh when running letsencrypt: 'echo "{\"certbot\": true}" | sudo chef-client -j /dev/stdin'
if node['certbot']
    execute 'Enable ssl' do
        command 'sudo a2enmod ssl'
    end
    
    execute 'Enable key caching' do
        command 'sudo a2enmod socache_shmcb'
    end
    
    execute 'Enable rewrite rules' do
        command 'sudo a2enmod rewrite'
    end
    
    execute 'Run letsencrypt to get a certificate' do
        command(
            "sudo /usr/local/sbin/certbot-auto certonly --apache -n" + 
            " -d #{droplet_data['domain']} -m #{droplet_data['to']} --agree-tos --redirect")
    end
    
    cookbook_file '/etc/letsencrypt/options-ssl-apache.conf' do
        source 'options-ssl-apache.conf'
    end
    
    template '/etc/apache2/sites-available/home-bistro-le-ssl.conf' do
    source 'home-bistro-le-ssl.conf.erb'
    action :create
    variables({
        :from => droplet_data['from'],
        :domain => droplet_data['domain'],
        :certificate_available => certificate_available
        })
    end
    
    execute 'Enable SSL Host' do
        command 'sudo a2ensite home-bistro-le-ssl.conf'
    end
    
    cron 'renew certificate' do
        minute '30'
        hour '2'
        user node['user_name']
        mailto droplet_data['to']
        command '/usr/local/sbin/certbot-auto renew >> /var/log/le-renew.log'
    end
end

service 'apache2' do
    action :restart
end
