#
# Cookbook Name:: digital_ocean
# Recipe:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

# This was built off an Ubuntu image, specifically Ubuntu 16.04.1 x64.

#
# Harden the server before deploying the requisite packages to the machines.
#

user_name = node['user_name']
user_home_dir = '/home/' + user_name
user user_name do
    manage_home true
    home user_home_dir
    shell '/bin/bash'
end
directory user_home_dir + '/.ssh' do
    owner user_name
    group user_name
    mode '700'
    action :create
end
file user_home_dir + '/.ssh/authorized_keys' do
    owner user_name
    group user_name
    mode '400'
    # Obviously, we're using the same key as for root.  This isn't expected to be shared with others, but can 
    # be modified for a new key if needed.
    content ::File.open('/root/.ssh/authorized_keys').read
    action :create
end
template '/etc/sudoers' do
    source 'sudoers.erb'
    variables({:user_name => user_name})
end

template '/etc/ssh/sshd_config' do
    source 'sshd_config.erb'
    variables({:user_name => user_name})    
end
service 'ssh' do
    action :restart
end

execute "Allow ssh access" do
    command "sudo ufw allow 22"
end
execute "Allow http access" do
    command "sudo ufw allow 80"
end
execute "Allow https access" do
    command "sudo ufw allow 443"
end
execute "Disable ufw" do
    command "sudo ufw --force disable"
end
execute "Enable ufw" do
    command "sudo ufw --force enable"
end

package 'unattended-upgrades'
cookbook_file '/etc/apt/apt.conf.d/10periodic' do
    source '10periodic'
end
cookbook_file '/etc/apt/apt.conf.d/50unattended-upgrades' do
    source '50unattended-upgrades'
end

droplet_data = data_bag_item('sysadmins','digital_ocean_droplet')

package 'fail2ban'
package 'logwatch'
template '/usr/share/logwatch/default.conf/logwatch.conf' do
    source 'logwatch.conf.erb'
    variables({
        :from => droplet_data['from'],
        :to => droplet_data['to']
        })
end
