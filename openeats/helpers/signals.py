from django.apps import apps
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.core.mail import mail_admins


def createUserProfile(sender, instance, **kwargs):
    """Listens to the save signal of the User class then 
       create a UserProfile object each time a User is activated ; and link it.
    """
    user_profiles = apps.get_model('accounts','UserProfiles')
    user_profiles.objects.get_or_create(user=instance)

post_save.connect(createUserProfile, sender=User)


def notifyAdminReportedRecipe(sender, instance, created, **kwargs):
    """Listens to the save signal for the report recipe class and
        sends an email to the admins
    """
    if created:
        subject = "A recipe has been reported"
        message = "Recipe %s was reported as being inappropriate" % instance.recipe
        mail_admins(subject, message)

reported_recipe = apps.get_model('recipe','ReportedRecipe')
post_save.connect(notifyAdminReportedRecipe, sender=reported_recipe)