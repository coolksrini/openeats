from django.conf.urls import url
from openeats.news import views as news_views

urlpatterns = [
    url(r'^list/$', news_views.list, name="news_list"),
    url(r'^entry/(?P<slug>[\w-]+)/$', news_views.entry, name="news_entry")]