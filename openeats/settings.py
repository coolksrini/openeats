import os

### Site basics ###
ALLOWED_HOSTS = [os.environ.get('ALLOWED_HOSTS','')]
ROOT_URLCONF = 'openeats.urls'
BASE_PATH = os.path.dirname(os.path.abspath(__file__))
SITE_ID = 1
CACHE_BACKEND = "file://"+os.path.join(BASE_PATH, 'cache')
# Make this unique, and don't share it with anybody.
SECRET_KEY = 'tk1ig_pa_p9^muz4vw4%#q@0no$=ce1*b$#s343jouyq9lj)k33j('
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', 
        'NAME': 'openeats.db',           
    }
}


### OpenEats2 settings ###
OELOGO = 'images/oelogo.png'
OETITLE = 'OpenEats2 Dev'


### Admin related functionality ###
ADMINS = (
    # ('Your Name', 'youremail@email.com'),
)
MANAGERS = ADMINS
GRAPPELLI_ADMIN_TITLE = OETITLE
GRAPPELLI_INDEX_DASHBOARD = 'openeats.dashboard.CustomIndexDashboard'


LANGUAGE_CODE = 'en-us'
USE_I18N = True
### Internationalization and localization ###
USE_L10N = True
LOCALE_PATHS = (
  os.path.join(BASE_PATH, 'locale',),
)
ugettext = lambda s: s
LANGUAGES = (
     ('en', ugettext('English')),
     ('de', ugettext('German')),
     ('es', ugettext('Spanish')),
   )
TIME_ZONE = 'America/Chicago'


### Static and media files ###
MEDIA_ROOT = os.path.join(BASE_PATH, 'site-media')
STATIC_ROOT = os.path.join(BASE_PATH, 'static-files')
MEDIA_URL = '/media/'
STATIC_URL = '/static/'
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.FileSystemFinder',
)
SERVE_MEDIA = True

### Debug settings ###
DEBUG_TOOLBAR_PATCH_SETTINGS = False
DEBUG_TOOLBAR_CONFIG = {
'INTERCEPT_REDIRECTS': False,
}
INTERNAL_IPS = ('127.0.0.1',)
DEBUG = True


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_PATH,'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                "openeats.navbar.context_processors.navbars",
                "openeats.context_processors.oelogo",
                "openeats.context_processors.oetitle",
            ],
        },
    },
]

### Middleware and Applications ###
MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

INSTALLED_APPS = (
    'grappelli.dashboard',
    'grappelli',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.admin.apps.SimpleAdminConfig',
    'django.contrib.flatpages',
    'django.contrib.staticfiles',
    'debug_toolbar',
    'taggit', 
    'disqus',
    'rosetta',
    'registration',
    'imagekit',
    'haystack',
    'django_extensions',
    'tastypie',
    'openeats',
    'openeats.navbar',
    'openeats.recipe',
    'openeats.recipe_groups',
    'openeats.ingredient',
    'openeats.accounts',
    'openeats.news',
    'openeats.list',
)


### Email server settings ###
DEFAULT_FROM_EMAIL = os.environ.get('EMAIL_HOST_USER','')
EMAIL_HOST = os.environ.get('EMAIL_HOST','')
EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER','')
EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD','')
EMAIL_PORT = os.environ.get('EMAIL_PORT','')


### Registration and Auth ###
LOGIN_REDIRECT_URL = "/recipe/"
REGISTRATION_OPEN = True
ACCOUNT_ACTIVATION_DAYS=7


#### Search configuration ###
HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'
HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH':    os.path.join(BASE_PATH, 'search_index')
    }
}

try:
    from local_settings import *
except ImportError:
    pass
