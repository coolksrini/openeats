from django.contrib.auth.models import User
from django.test import Client,TestCase

from ..recipe.models import Recipe
#from ..recipe.views import 

class TestUserRecipesView(TestCase):
    def setUp(self):
        user1 = User.objects.create_user('user1','user1@example.com','password1')
        user2 = User.objects.create_user('user2','user2@example.com','password2')
        Recipe.objects.create(title="Delicious Recipe",author=user1)
        Recipe.objects.create(title="Fine Dining Recipe",author=user2)
        #self.userRecipesView = UserRecipesView()
        #self.client = Client(follow=True)
    
    def test_no_results_for_anonymous_users(self):
        response = self.client.get('/recipe/')
        self.assertEqual(302,response.status_code)
        self.assertEqual('/accounts/login/?next=/recipe/',response.url)
        
    def test_results_for_logged_in_users(self):
        self.client.login(username='user1',password='password1')
        response = self.client.get('/recipe/')
        self.assertEqual(200,response.status_code)
        self.assertContains(response,'Delicious Recipe')
        self.assertNotContains(response,'Fine Dining Recipe')