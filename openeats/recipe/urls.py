from django.conf.urls import *

import openeats.recipe.views as recipe_views

urlpatterns = [
    url(r'^new/$', recipe_views.recipe, name="new_recipe"),
    url(r'^mail/(?P<id>\d+)/$', recipe_views.recipeMail, name='recipe_mail'),
    url(r'^edit/(?P<user>[-\w]+)/(?P<slug>[-\w]+)/$', recipe_views.recipe, name='recipe_edit'),
    url(r'^print/(?P<slug>[-\w]+)/$', recipe_views.recipePrint, name="print_recipe"),
    url(r'^cook/(?P<slug>[-\w]+)/$', recipe_views.CookList.as_view()),
    url(r'^report/(?P<slug>[-\w]+)/$', recipe_views.recipeReport, name='recipe_report'),
    url(r'^store/(?P<object_id>\d+)/$', recipe_views.recipeStore, name='recipe_store'),
    url(r'^unstore/$', recipe_views.recipeUnStore, name='recipe_unstore'),
    url(r'^ajaxnote/$', recipe_views.recipeNote),
    url(r'^ajaxulist/(?P<shared>[-\w]+)/(?P<user>[-\w]+)/$', recipe_views.recipeUser),
    url(r'^ajax-raterecipe/(?P<object_id>\d+)/(?P<score>\d+)/$', recipe_views.recipeRate, name='recipe_rate'),
    url(r'^ajax-favrecipe/$', recipe_views.recipeUserFavs),
    url(r'^recent/$', recipe_views.RecentRecipeView.as_view(), name='recipe_recent'),
    url(r'^(?P<id>\d+)/$', recipe_views.recipeShow, name='recipe_show'),
    url(r'^export/(?P<slug>[-\w]+)/$', recipe_views.exportPDF, name='recipe_export'),
    url(r'^$', recipe_views.user_recipes, name='recipe_index')]
